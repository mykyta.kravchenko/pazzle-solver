function solvePuzzle(pieces) {
  const solvedPuzzle = [];
  const sidePuzzleCount = Math.sqrt(pieces.length);
  const piecesClone = JSON.parse(JSON.stringify(pieces));

  function rotatePuzzle(puzzle) {
    [
      puzzle.edges.top, puzzle.edges.right, puzzle.edges.bottom, puzzle.edges.left
    ] = [
      puzzle.edges.right, puzzle.edges.bottom, puzzle.edges.left, puzzle.edges.top
    ];
  }

  function findNeighborIndex(array, { edgeTypeId, type }) {
    return array.findIndex(({ edges }) => {
      let found = false;

      for (const edge in edges) {
        if (edges[edge]?.edgeTypeId === edgeTypeId && edges[edge].type !== type) {
          found = true;
        }
      }

      return found;
    });
  }

  function swapNeighbor(edge, side, i) {
    const NeighborIndex = findNeighborIndex(piecesClone, edge);

    const { edges } = piecesClone[NeighborIndex];

    while (edge.edgeTypeId !== edges[side]?.edgeTypeId) {
      rotatePuzzle(piecesClone[NeighborIndex]);
    }

    [piecesClone[i + 1], piecesClone[NeighborIndex]] = [piecesClone[NeighborIndex], piecesClone[i + 1]];
  }

  while (piecesClone[0].edges.top || piecesClone[0].edges.left) {
    rotatePuzzle(piecesClone[0]);
  }

  for (let i = 0; i < piecesClone.length - 1; i++) {
    const rightEdge = piecesClone[i].edges.right;
    solvedPuzzle.push(piecesClone[i].id);

    if (!rightEdge) {
      const bottomEdge = piecesClone[i - sidePuzzleCount + 1].edges.bottom;
      swapNeighbor(bottomEdge, 'top', i);
    } else {
      swapNeighbor(rightEdge, 'left', i);
    }
  }

  solvedPuzzle.push(piecesClone[piecesClone.length - 1].id);

  return solvedPuzzle;
}

// Не удаляйте эту строку
window.solvePuzzle = solvePuzzle;
